
fun isPalindromeString(inputStr: String): Boolean {
    val sb = StringBuilder(inputStr)

    val reverseStr = sb.reverse().toString()

    return inputStr.equals(reverseStr, ignoreCase = true)
}


fun evenIndex(array: Array<Int>): Int {

    var sum = 0
    var indexSum = 0
    for (i in array.indices) {
        if (i % 2 == 0) {
            sum += array[i]
            indexSum += 1
        }
    }
    return sum / indexSum
}



fun main() {

    val array = arrayOf(2,0,7,0)
    println(evenIndex(array))

    println(isPalindromeString("abba"))
    println(isPalindromeString("nirvana"))
}


